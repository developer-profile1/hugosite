---
title: "About"
description: "A concise narrative of my existence, the architect of this digital realm."
featured_image: ''
tags: ["javascript", "golang", "nodejs", "electron", "python", "dart", "flutter", "web3", "blockchain", "api", "packages", "libraries", "repositories"]
---

Welcome!

I am excited to share my journey as a software engineer with you!

My name is Dimitri Molokov, and this website is a reflection of my professional journey. I created it to showcase my skills and build a portfolio that will help me land a job in my field.

As a software engineer, I am known for my ability to quickly learn and adapt to new environments and team dynamics. I have a wide range of programming skills, including web, mobile, and backend development. I have experience with various programming languages, including JavaScript, Golang, NodeJS, Electron, Python, Dart, Flutter, and others that are less common but still interesting.

My passion is creating software for desktop, mobile, and web applications. I love the challenge of developing new things and leaving a lasting impression on users with my work.

I am open to both open-source and commercial software development projects. If you need an experienced developer for your project, feel free to contact me.